package cz.karotkin.eshopbackend.mother;

import cz.karotkin.eshopbackend.domain.Cart;
import cz.karotkin.eshopbackend.model.CartDto;

public class CartMother {

    public static Cart prepareCart() {
        return (Cart) new Cart()
                .setId(1L);
    }

    public static CartDto prepareCartDto() {
        return new CartDto()
                .setId(1L);
    }
}
