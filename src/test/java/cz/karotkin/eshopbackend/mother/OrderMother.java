package cz.karotkin.eshopbackend.mother;

import cz.karotkin.eshopbackend.domain.Order;
import cz.karotkin.eshopbackend.model.OrderDto;

public class OrderMother {

    public static Order prepareOrder() {
        return (Order) new Order()
                .setId(1L);
    }

    public static OrderDto prepareOrderDto() {
        return new OrderDto()
                .setId(1L);
    }
}
