package cz.karotkin.eshopbackend.mapper;

import cz.karotkin.eshopbackend.domain.Author;
import cz.karotkin.eshopbackend.model.AuthorDto;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static cz.karotkin.eshopbackend.mother.AuthorMother.prepareAuthor;

class AuthorMapperTest implements WithAssertions {

    private final AuthorMapper authorMapper = Mappers.getMapper(AuthorMapper.class);

    @Test
    void toDto() {
        Author author = prepareAuthor();
        AuthorDto result = authorMapper.toDto(author);

        assertThat(result.getId()).isEqualTo(author.getId());
        assertThat(result.getName()).isEqualTo(author.getName());
        assertThat(result.getBio()).isEqualTo(author.getBio());
        assertThat(result.getBirthDate()).isEqualTo(author.getBirthDate());
    }
}