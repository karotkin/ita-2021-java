package cz.karotkin.eshopbackend.mapper;

import cz.karotkin.eshopbackend.domain.Cart;
import cz.karotkin.eshopbackend.model.CartDto;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static cz.karotkin.eshopbackend.mother.CartMother.prepareCart;

class CartMapperTest implements WithAssertions {

    private final CartMapper cartMapper = Mappers.getMapper(CartMapper.class);

    @Test
    void toDto() {
        Cart cart = prepareCart();
        CartDto result = cartMapper.toDto(cart);

        assertThat(result.getId()).isEqualTo(cart.getId());
        assertThat(result.getProducts()).isEqualTo(cart.getProducts());
    }
}