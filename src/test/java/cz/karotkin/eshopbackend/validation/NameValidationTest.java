package cz.karotkin.eshopbackend.validation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

class NameValidationTest {

    @Test
    void isValid() {
        final NameValidation validation = new NameValidation();
        String input = "Valid";
        boolean result = validation.isValid(input, null);
        assertThat(result).isTrue();
    }

    @Test
    void isInvalid() {
        final NameValidation validation = new NameValidation();
        String input = "invalid";
        boolean result = validation.isValid(input, null);
        assertThat(result).isFalse();
    }

    @ParameterizedTest
    @ValueSource(strings = {"Dominik", "Validní", "LENKA", "Dobré"})
    void isValid_ShouldReturnTrueForValidStrings(String value) {
        final NameValidation validation = new NameValidation();
        assertTrue(validation.isValid(value, null));
    }
}