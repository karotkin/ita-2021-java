package cz.karotkin.eshopbackend.repository;

import cz.karotkin.eshopbackend.domain.Author;
import cz.karotkin.eshopbackend.domain.Genre;
import cz.karotkin.eshopbackend.domain.Product;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.time.LocalDate;
import java.time.Month;
import java.util.Optional;

@DataJpaTest
class ProductRepositoryIT implements WithAssertions {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    void createAndRetrieveProduct() {

        Author author = new Author()
                .setName("Dominik Bican")
                .setBio("Narozen v Písku")
                .setBirthDate(LocalDate.of(1986, Month.APRIL, 13));

        Genre genre = new Genre()
                .setName("Detektivka")
                .setDescription("Vražedné motivy");

        Product product = new Product()
                .setName("Smrt")
                .setDescription("Kniha o vraždách")
                .setPrice(500L)
                .setStock(20L)
                .setImage("xxx")
                .setAuthor(author)
                .setGenre(genre);

        testEntityManager.persistAndFlush(author);
        testEntityManager.persistAndFlush(genre);
        testEntityManager.persistAndFlush(product);
        testEntityManager.detach(product);

        Optional<Product> result = productRepository.findById(product.getId());

        assertThat(result).isNotEmpty();

        Product resultProduct = result.get();

        assertThat(resultProduct).usingRecursiveComparison().isEqualTo(product);
    }

}