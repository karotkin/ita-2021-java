package cz.karotkin.eshopbackend.repository;

import cz.karotkin.eshopbackend.domain.Order;
import cz.karotkin.eshopbackend.domain.Product;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;
import java.util.Optional;

@DataJpaTest
class OrderRepositoryIT implements WithAssertions {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    void createAndRetrieveOrder() {

        Product product1 = new Product();
        Product product2 = new Product();
        List<Product> products = List.of(product1, product2);

        Order order = new Order()
                .setProducts(products);

        testEntityManager.persistAndFlush(product1);
        testEntityManager.persistAndFlush(product2);
        testEntityManager.persistAndFlush(order);
        testEntityManager.detach(order);

        Optional<Order> result = orderRepository.findById(order.getId());

        assertThat(result).isNotEmpty();

        Order resultOrder = result.get();

        assertThat(resultOrder.getStatus()).isEqualTo(order.getStatus());
        assertThat(resultOrder.getProducts().get(0)).isEqualTo(order.getProducts().get(0));
        assertThat(resultOrder.getProducts().get(1)).isEqualTo(order.getProducts().get(1));
    }
}