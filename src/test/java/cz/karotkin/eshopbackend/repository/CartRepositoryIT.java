package cz.karotkin.eshopbackend.repository;

import cz.karotkin.eshopbackend.domain.Cart;
import cz.karotkin.eshopbackend.domain.Product;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;
import java.util.Optional;

@DataJpaTest
class CartRepositoryIT implements WithAssertions {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    void createAndRetrieveCart() {

        Product product1 = new Product();
        Product product2 = new Product();
        List<Product> products = List.of(product1, product2);

        Cart cart = new Cart()
                .setProducts(products);

        testEntityManager.persistAndFlush(product1);
        testEntityManager.persistAndFlush(product2);
        testEntityManager.persistAndFlush(cart);
        testEntityManager.detach(cart);

        Optional<Cart> result = cartRepository.findById(cart.getId());

        assertThat(result).isNotEmpty();

        Cart resultCart = result.get();

        assertThat(resultCart.getProducts().get(0)).isEqualTo(cart.getProducts().get(0));
        assertThat(resultCart.getProducts().get(1)).isEqualTo(cart.getProducts().get(1));
    }

}