package cz.karotkin.eshopbackend.rest;

import cz.karotkin.eshopbackend.configuration.SecurityConfiguration;
import cz.karotkin.eshopbackend.configuration.SecurityConfigurationProperties;
import org.springframework.context.annotation.Import;

@Import({SecurityConfiguration.class, SecurityConfigurationProperties.class})
public abstract class AbstractControllerTest {
}
