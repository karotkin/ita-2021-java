package cz.karotkin.eshopbackend.rest;

import cz.karotkin.eshopbackend.exception.ProductNotFoundException;
import cz.karotkin.eshopbackend.model.ProductDto;
import cz.karotkin.eshopbackend.model.ProductRequestDto;
import cz.karotkin.eshopbackend.model.ProductSimpleDto;
import cz.karotkin.eshopbackend.service.ProductService;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static cz.karotkin.eshopbackend.mother.ProductMother.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
class ProductControllerTest extends AbstractControllerTest implements WithAssertions {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ProductService productService;

    @Test
    void findProduct() throws Exception {
        ProductDto productDto = prepareProductDto();

        when(productService.findProduct(1L))
                .thenReturn(productDto);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(getJsonContent("/responses/findProduct.json")));

        verify(productService).findProduct(1L);
    }

    @Test
    void findProduct_notFound() throws Exception {

        when(productService.findProduct(1L))
                .thenThrow(new ProductNotFoundException(1L));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products/1"))
                .andExpect(status().isNotFound())
                .andExpect(content().json(getJsonContent("/responses/findProduct_notFound.json")));

        verify(productService).findProduct(1L);
    }

    @Test
    void findAllProducts() throws Exception {

        List<ProductSimpleDto> productsSimpleDto = prepareProductsSimpleDto();

        when(productService.findAllProducts())
                .thenReturn(productsSimpleDto);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/products"))
                .andExpect(status().isOk())
                .andExpect(content().json(getJsonContent("/responses/findAllProducts.json")));

        verify(productService).findAllProducts();
    }

    @Test
    void createProduct() throws Exception {

        ProductRequestDto productRequestDto = prepareProductRequestDto();
        ProductDto productDto = prepareProductDto();
        when(productService.createProduct(productRequestDto))
                .thenReturn(productDto);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Basic a2Fyb3RraW46cGFzc3dvcmQ=");
        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/products")
                        .headers(httpHeaders)
                        .contentType("application/json")
                        .content(getJsonContent("/requests/createProduct.json")))
                .andExpect(status().isOk())
                .andExpect(content().json(getJsonContent("/responses/createProduct.json")));

        verify(productService).createProduct(productRequestDto);
    }

    @Test
    void editProduct() throws Exception {

        ProductRequestDto productRequestDto = prepareProductRequestDto();
        ProductDto productDto = prepareProductDto();
        when(productService.editProduct(2L, productRequestDto))
                .thenReturn(productDto);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Basic a2Fyb3RraW46cGFzc3dvcmQ=");
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/products/2")
                        .headers(httpHeaders)
                        .contentType("application/json")
                        .content(getJsonContent("/requests/editProduct.json")))
                .andExpect(status().isOk())
                .andExpect(content().json(getJsonContent("/responses/editProduct.json")));

        verify(productService).editProduct(2L, productRequestDto);
    }

    @Test
    void editProduct_notFound() throws Exception {

        ProductRequestDto productRequestDto = prepareProductRequestDto();

        when(productService.editProduct(2L, productRequestDto))
                .thenThrow(new ProductNotFoundException(2L));

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Basic a2Fyb3RraW46cGFzc3dvcmQ=");
        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/products/2")
                        .headers(httpHeaders)
                        .contentType("application/json")
                        .content(getJsonContent("/requests/editProduct.json")))
                .andExpect(status().isNotFound())
                .andExpect(content().json(getJsonContent("/responses/editProduct_notFound.json")));

        verify(productService).editProduct(2L, productRequestDto);
    }



    @Test
    void deleteProduct() throws Exception {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Basic a2Fyb3RraW46cGFzc3dvcmQ=");
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/products/1")
                .headers(httpHeaders))
                .andExpect(status().isNoContent());

        verify(productService).deleteProduct(1L);
    }

    private String getJsonContent(String resource) throws IOException, URISyntaxException {
        return Files.readString(Paths.get(getClass().getResource(resource).toURI()));
    }
}