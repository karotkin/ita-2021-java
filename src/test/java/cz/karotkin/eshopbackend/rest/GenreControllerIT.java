package cz.karotkin.eshopbackend.rest;

import cz.karotkin.eshopbackend.model.GenreDto;
import cz.karotkin.eshopbackend.repository.GenreRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GenreControllerIT implements WithAssertions {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private GenreRepository genreRepository;

    @Test
    void findAllGenres() {

        ResponseEntity<GenreDto[]> response = testRestTemplate.getForEntity("/api/v1/genres", GenreDto[].class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        GenreDto[] body = response.getBody();

        assertThat(body).hasSize(2);
    }
}