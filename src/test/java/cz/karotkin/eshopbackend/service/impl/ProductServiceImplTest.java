package cz.karotkin.eshopbackend.service.impl;

import cz.karotkin.eshopbackend.domain.Product;
import cz.karotkin.eshopbackend.exception.ProductNotFoundException;
import cz.karotkin.eshopbackend.mapper.ProductMapper;
import cz.karotkin.eshopbackend.model.ProductDto;
import cz.karotkin.eshopbackend.model.ProductRequestDto;
import cz.karotkin.eshopbackend.model.ProductSimpleDto;
import cz.karotkin.eshopbackend.repository.AuthorRepository;
import cz.karotkin.eshopbackend.repository.GenreRepository;
import cz.karotkin.eshopbackend.repository.ProductRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static cz.karotkin.eshopbackend.mother.AuthorMother.prepareAuthor;
import static cz.karotkin.eshopbackend.mother.GenreMother.prepareGenre;
import static cz.karotkin.eshopbackend.mother.ProductMother.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest implements WithAssertions {

    @InjectMocks
    private ProductServiceImpl productService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private GenreRepository genreRepository;

    @Mock
    private ProductMapper productMapper;

    @Test
    void findAllProducts() {
        List<Product> products = prepareProducts();
        when(productRepository.findAll())
                .thenReturn(products);

        List<ProductSimpleDto> expectedResult = prepareProductsSimpleDto();
        when(productMapper.toSimpleDto(products.get(0)))
                .thenReturn(expectedResult.get(0));
        when(productMapper.toSimpleDto(products.get(1)))
                .thenReturn(expectedResult.get(1));

        Collection<ProductSimpleDto> result = productService.findAllProducts();

        assertThat(result).isEqualTo(expectedResult);

        verify(productRepository).findAll();
        verify(productMapper).toSimpleDto(products.get(0));
        verify(productMapper).toSimpleDto(products.get(1));
    }

    @Test
    void findProduct() {
        Product product = prepareProduct();
        when(productRepository.findById(1L))
                .thenReturn(Optional.of(product));

        ProductDto expectedResult = prepareProductDto();
        when(productMapper.toDto(product))
                .thenReturn(expectedResult);

        ProductDto result = productService.findProduct(1L);

        assertThat(result).isEqualTo(expectedResult);

        verify(productRepository).findById(1L);
        verify(productMapper).toDto(product);
    }

    @Test
    void findProduct_productNotFound() {
        when(productRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> {
            productService.findProduct(1L);
        });

        verifyNoInteractions(productMapper);
    }

    @Test
    void createProduct() {
        ProductRequestDto productRequestDto = prepareProductRequestDto();
        Product product = prepareProduct();
        when(productMapper.toDomain(productRequestDto))
                .thenReturn(product);

        ProductDto expectedResult = prepareProductDto();
        when(productMapper.toDto(product))
                .thenReturn(expectedResult);

        when(authorRepository.findById(productRequestDto.getAuthorId()))
                .thenReturn(Optional.of(prepareAuthor()));
        when(genreRepository.findById(productRequestDto.getGenreId()))
                .thenReturn(Optional.of(prepareGenre()));

        ProductDto result = productService.createProduct(productRequestDto);

        assertThat(expectedResult).isEqualTo(result);

        verify(productMapper).toDomain(productRequestDto);
        verify(productMapper).toDto(product);
        verify(productRepository).save(product);
    }

    @Test
    void editProduct() {
        Product product = prepareProduct();
        when(productRepository.findById(1L))
                .thenReturn(Optional.of(product));

        ProductRequestDto productRequestDto = prepareProductRequestDto();
        ProductDto expectedResult = prepareProductDto();
        when(productMapper.toDto(product))
                .thenReturn(expectedResult);

        when(authorRepository.findById(productRequestDto.getAuthorId()))
                .thenReturn(Optional.of(prepareAuthor()));
        when(genreRepository.findById(productRequestDto.getGenreId()))
                .thenReturn(Optional.of(prepareGenre()));

        ProductDto result = productService.editProduct(1L, productRequestDto);

        assertThat(expectedResult).isEqualTo(result);

        verify(productMapper).mergeProduct(product, productRequestDto);
        verify(productMapper).toDto(product);
        verify(productRepository).findById(1L);
    }

    @Test
    void editProduct_productNotFound() {
        ProductRequestDto productRequestDto = prepareProductRequestDto();
        when(productRepository.findById(1L))
                .thenReturn(Optional.empty());

        assertThrows(ProductNotFoundException.class, () -> {
            productService.editProduct(1L, productRequestDto);
        });

        verifyNoInteractions(productMapper);
    }

    @Test
    void deleteProduct() {
        when(productRepository.existsById(1L))
                .thenReturn(true);
        productService.deleteProduct(1L);
        verify(productRepository).deleteById(1L);
    }

    @Test
    void deleteProduct_productNotFound() {
        when(productRepository.existsById(1L))
                .thenReturn(false);
        assertThrows(ProductNotFoundException.class, () -> {
            productService.deleteProduct(1L);
        });
    }
}