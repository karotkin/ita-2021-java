package cz.karotkin.eshopbackend.service.impl;

import cz.karotkin.eshopbackend.domain.Author;
import cz.karotkin.eshopbackend.mapper.AuthorMapper;
import cz.karotkin.eshopbackend.model.AuthorDto;
import cz.karotkin.eshopbackend.repository.AuthorRepository;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static cz.karotkin.eshopbackend.mother.AuthorMother.prepareAuthors;
import static cz.karotkin.eshopbackend.mother.AuthorMother.prepareAuthorsDto;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthorServiceImplTest implements WithAssertions {

    @InjectMocks
    private AuthorServiceImpl authorService;

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private AuthorMapper authorMapper;

    @Test
    void findAllAuthors() {
        List<Author> authors = prepareAuthors();
        List<AuthorDto> expectedResult = prepareAuthorsDto();

        when(authorRepository.findAll())
                .thenReturn(authors);
        when(authorMapper.toDto(authors.get(0)))
                .thenReturn(expectedResult.get(0));
        when(authorMapper.toDto(authors.get(1)))
                .thenReturn(expectedResult.get(1));

        List<AuthorDto> result = authorService.findAllAuthors();

        assertThat(result).isEqualTo(expectedResult);
        verify(authorRepository).findAll();
        verify(authorMapper).toDto(authors.get(0));
        verify(authorMapper).toDto(authors.get(1));
    }
}