package cz.karotkin.eshopbackend.exception;

import org.springframework.http.HttpStatus;

public class ProductNotFoundException extends ItaException {
    public ProductNotFoundException(Long id) {
        super(String.format("Product %d not found!", id), "0001", HttpStatus.NOT_FOUND);
    }
}
