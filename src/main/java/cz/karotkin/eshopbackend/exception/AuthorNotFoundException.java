package cz.karotkin.eshopbackend.exception;

import org.springframework.http.HttpStatus;

public class AuthorNotFoundException extends ItaException {
    public AuthorNotFoundException(Long id) {
        super(String.format("Author %d not found!", id), "0001", HttpStatus.NOT_FOUND);
    }
}
