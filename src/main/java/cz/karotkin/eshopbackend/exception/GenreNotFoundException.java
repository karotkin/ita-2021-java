package cz.karotkin.eshopbackend.exception;

import org.springframework.http.HttpStatus;

public class GenreNotFoundException extends ItaException {
    public GenreNotFoundException(Long id) {
        super(String.format("Genre %d not found!", id), "0001", HttpStatus.NOT_FOUND);
    }
}
