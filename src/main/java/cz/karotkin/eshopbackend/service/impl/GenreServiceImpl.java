package cz.karotkin.eshopbackend.service.impl;

import cz.karotkin.eshopbackend.mapper.GenreMapper;
import cz.karotkin.eshopbackend.model.GenreDto;
import cz.karotkin.eshopbackend.repository.GenreRepository;
import cz.karotkin.eshopbackend.service.GenreService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GenreServiceImpl implements GenreService {

    private final GenreRepository genreRepository;
    private final GenreMapper genreMapper;

    @Override
    @Transactional(readOnly = true)
    public List<GenreDto> findAllGenres() {
        return genreRepository.findAll().stream()
                .map(genreMapper::toDto)
                .collect(Collectors.toList());
    }
}
