package cz.karotkin.eshopbackend.service.impl;

import cz.karotkin.eshopbackend.mapper.AuthorMapper;
import cz.karotkin.eshopbackend.model.AuthorDto;
import cz.karotkin.eshopbackend.repository.AuthorRepository;
import cz.karotkin.eshopbackend.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;

    @Override
    @Transactional(readOnly = true)
    public List<AuthorDto> findAllAuthors() {
        return authorRepository.findAll().stream()
                .map(authorMapper::toDto)
                .collect(Collectors.toList());
    }
}
