package cz.karotkin.eshopbackend.service.impl;

import cz.karotkin.eshopbackend.domain.Cart;
import cz.karotkin.eshopbackend.domain.Order;
import cz.karotkin.eshopbackend.domain.Product;
import cz.karotkin.eshopbackend.exception.CartNotFoundException;
import cz.karotkin.eshopbackend.exception.OrderNotFoundException;
import cz.karotkin.eshopbackend.mapper.OrderMapper;
import cz.karotkin.eshopbackend.model.OrderDto;
import cz.karotkin.eshopbackend.model.OrderStatus;
import cz.karotkin.eshopbackend.repository.CartRepository;
import cz.karotkin.eshopbackend.repository.OrderRepository;
import cz.karotkin.eshopbackend.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final CartRepository cartRepository;
    private final OrderMapper orderMapper;

    @Override
    @Transactional
    public OrderDto createOrder(Long cartId) {
        log.info("Creating order from cart id " + cartId);

        Cart cart = cartRepository.findById(cartId)
                .orElseThrow(() -> new CartNotFoundException(cartId));
        List<Product> products = new ArrayList<>(cart.getProducts());
        Order order = new Order()
                .setProducts(products)
                .setStatus(OrderStatus.NEW);
        orderRepository.save(order);

        OrderDto result = orderMapper.toDto(order);
        log.debug("Created order: {}", result);
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrderDto> findAllOrders() {
        return orderRepository.findAll().stream()
                .map(orderMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public OrderDto updateOrderStatus(Long orderId, String status) {
        Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new OrderNotFoundException(orderId));
        order.setStatus(OrderStatus.valueOf(status));
        OrderDto result = orderMapper.toDto(order);
        return result;
    }
}
