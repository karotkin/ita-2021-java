package cz.karotkin.eshopbackend.service.impl;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import cz.karotkin.eshopbackend.configuration.AmazonConfig;
import cz.karotkin.eshopbackend.domain.Author;
import cz.karotkin.eshopbackend.domain.Genre;
import cz.karotkin.eshopbackend.domain.Product;
import cz.karotkin.eshopbackend.exception.AuthorNotFoundException;
import cz.karotkin.eshopbackend.exception.FileNotReadableException;
import cz.karotkin.eshopbackend.exception.GenreNotFoundException;
import cz.karotkin.eshopbackend.exception.ProductNotFoundException;
import cz.karotkin.eshopbackend.feign.WarehouseClient;
import cz.karotkin.eshopbackend.mapper.ProductMapper;
import cz.karotkin.eshopbackend.model.PreviewResponse;
import cz.karotkin.eshopbackend.model.ProductDto;
import cz.karotkin.eshopbackend.model.ProductRequestDto;
import cz.karotkin.eshopbackend.model.ProductSimpleDto;
import cz.karotkin.eshopbackend.repository.AuthorRepository;
import cz.karotkin.eshopbackend.repository.GenreRepository;
import cz.karotkin.eshopbackend.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements cz.karotkin.eshopbackend.service.ProductService {

    private final ProductRepository productRepository;
    private final AuthorRepository authorRepository;
    private final GenreRepository genreRepository;
    private final ProductMapper productMapper;
    private final AmazonS3 amazonS3;
    private final AmazonConfig amazonConfig;
    private final WarehouseClient client;

    @Override
    @Transactional(readOnly = true)
    public List<ProductSimpleDto> findAllProducts() {
        return productRepository.findAll().stream()
                .map(productMapper::toSimpleDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public ProductDto findProduct(Long id) {
        return productRepository.findById(id)
                .map(productMapper::toDto)
                .orElseThrow(() -> new ProductNotFoundException(id));
    }

    @Override
    @Transactional
    public ProductDto createProduct(ProductRequestDto productRequestDto) {
        log.info("Creating product");
        log.debug("Incoming payload: {}", productRequestDto);

        Author author = authorRepository.findById(productRequestDto.getAuthorId())
                .orElseThrow(() -> new AuthorNotFoundException(productRequestDto.getAuthorId()));

        Genre genre = genreRepository.findById(productRequestDto.getGenreId())
                .orElseThrow(() -> new GenreNotFoundException(productRequestDto.getGenreId()));

        Product product = productMapper.toDomain(productRequestDto);
        product.setAuthor(author);
        product.setGenre(genre);
        productRepository.save(product);
        ProductDto result = productMapper.toDto(product);
        log.debug("Created product: {}", result);
        return result;
    }

    @Override
    @Transactional
    public ProductDto editProduct(Long id, ProductRequestDto productRequestDto) {
        log.info("Updating product");
        log.debug("Incoming payload: {}", productRequestDto);

        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException(id));

        Author author = authorRepository.findById(productRequestDto.getAuthorId())
                .orElseThrow(() -> new AuthorNotFoundException(productRequestDto.getAuthorId()));

        Genre genre = genreRepository.findById(productRequestDto.getGenreId())
                .orElseThrow(() -> new GenreNotFoundException(productRequestDto.getGenreId()));

        product.setAuthor(author);
        product.setGenre(genre);

        productMapper.mergeProduct(product, productRequestDto);

        ProductDto result = productMapper.toDto(product);
        log.debug("Updated product: {}", result);
        return result;
    }

    @Override
    @Transactional
    public void deleteProduct(Long id) {
        if(productRepository.existsById(id)) {
            productRepository.deleteById(id);
        } else {
            throw new ProductNotFoundException(id);
        }
    }

    @Override
    @Transactional
    public void addPreview(Long id, MultipartFile file) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException(id));

        String filename = product.getId() + "_" + file.getOriginalFilename();

        try {
            amazonS3.putObject(amazonConfig.getBucketName(),
                    filename,
                    file.getInputStream(),
                    new ObjectMetadata());
        } catch (IOException e) {
            throw new FileNotReadableException();
        }

        if (product.getPreviewFileName() != null) {
            amazonS3.deleteObject(amazonConfig.getBucketName(), product.getPreviewFileName());
        }

        product.setPreviewFileName(filename);
    }

    @Override
    public PreviewResponse getPreview(Long id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ProductNotFoundException(id));
        String filename = product.getPreviewFileName();

        try {
            S3Object object = amazonS3.getObject(amazonConfig.getBucketName(), filename);
            S3ObjectInputStream objectContent = object.getObjectContent();
            return new PreviewResponse()
                    .setFilename(filename)
                    .setBytes(IOUtils.toByteArray(objectContent));
        } catch (AmazonServiceException | IOException e) {
            throw new FileNotReadableException();
        }
    }

    @Override
    @Transactional
    public void updateStock() {
        List<Product> products = productRepository.findAll();
        for(Product product : products) {
            product.setStock(client.getStock(product.getId()));
        }
    }

}
