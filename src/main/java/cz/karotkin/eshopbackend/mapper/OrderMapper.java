package cz.karotkin.eshopbackend.mapper;

import cz.karotkin.eshopbackend.domain.Order;
import cz.karotkin.eshopbackend.model.OrderDto;
import org.mapstruct.Mapper;

@Mapper(uses = ProductMapper.class)
public interface OrderMapper {
    OrderDto toDto(Order order);
}
