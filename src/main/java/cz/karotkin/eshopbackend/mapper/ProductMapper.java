package cz.karotkin.eshopbackend.mapper;

import cz.karotkin.eshopbackend.domain.Product;
import cz.karotkin.eshopbackend.model.ProductDto;
import cz.karotkin.eshopbackend.model.ProductRequestDto;
import cz.karotkin.eshopbackend.model.ProductSimpleDto;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(uses = {AuthorMapper.class, GenreMapper.class})
public interface ProductMapper {
    Product toDomain(ProductRequestDto productRequestDto);

    @BeforeMapping
    default void mapPreview(@MappingTarget ProductDto productDto, Product product) {
        productDto.setHasPreview(product.getPreviewFileName() != null);
    }

    @Mapping(target = "hasPreview", ignore = true)
    ProductDto toDto(Product product);

    ProductSimpleDto toSimpleDto(Product product);

    void mergeProduct(@MappingTarget Product target, ProductRequestDto source);
}
