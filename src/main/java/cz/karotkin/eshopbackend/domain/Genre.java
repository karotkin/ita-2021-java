package cz.karotkin.eshopbackend.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@SequenceGenerator(name="id_gen", sequenceName="genre_seq", allocationSize=1)
@Getter
@Setter
public class Genre extends AbstractEntity {
    private String name;
    private String description;
}