package cz.karotkin.eshopbackend.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@SequenceGenerator(name="id_gen", sequenceName="product_seq", allocationSize=1)
@Getter
@Setter
public class Product extends AbstractEntity {
    private String name;
    @Column(length = 512)
    private String description;
    private Long price;
    private Long stock;
    private String image;
    @ManyToOne
    @JoinColumn(name = "author_id")
    private Author author;
    @ManyToOne
    @JoinColumn(name = "genre_id")
    private Genre genre;
    @Column(name = "preview_file_name")
    private String previewFileName;
}
