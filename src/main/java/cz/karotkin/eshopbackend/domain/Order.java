package cz.karotkin.eshopbackend.domain;

import cz.karotkin.eshopbackend.model.OrderStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@SequenceGenerator(name="id_gen", sequenceName="order_seq", allocationSize=1)
@Getter
@Setter
@Table(name = "eshop_order")
public class Order extends AbstractEntity {
    @Enumerated(EnumType.STRING)
    private OrderStatus status;
    @ManyToMany
    @JoinTable(
            name = "r_order_product",
            joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id")
    )
    private List<Product> products;
}
