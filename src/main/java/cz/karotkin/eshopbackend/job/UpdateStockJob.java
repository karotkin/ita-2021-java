package cz.karotkin.eshopbackend.job;

import cz.karotkin.eshopbackend.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class UpdateStockJob {

    @Autowired
    private ProductService productService;

    @Scheduled(cron = "${app.job.update-product-stock.cron}")
    public void updateStock() {
        productService.updateStock();
    }
}
