package cz.karotkin.eshopbackend.repository;

import cz.karotkin.eshopbackend.domain.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.List;

public interface CartRepository extends JpaRepository<Cart, Long> {

    List<Cart> findAllByModifiedAtBefore(Instant timestamp);
}
