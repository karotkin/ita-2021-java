package cz.karotkin.eshopbackend.repository;

import cz.karotkin.eshopbackend.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
