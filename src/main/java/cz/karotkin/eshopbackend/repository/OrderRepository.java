package cz.karotkin.eshopbackend.repository;

import cz.karotkin.eshopbackend.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
