package cz.karotkin.eshopbackend.model;

import lombok.*;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AuthorDto {

    private Long id;
    private String name;
    private String bio;
    private LocalDate birthDate;

}
