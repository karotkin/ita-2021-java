package cz.karotkin.eshopbackend.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProductDto {

    private Long id;
    private String name;
    private String description;
    private Long price;
    private Long stock;
    private String image;
    private AuthorDto author;
    private GenreDto genre;
    private boolean hasPreview;

}
