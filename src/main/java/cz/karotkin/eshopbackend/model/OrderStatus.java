package cz.karotkin.eshopbackend.model;

public enum OrderStatus {
    NEW, COMPLETED, CANCELLED;
}
