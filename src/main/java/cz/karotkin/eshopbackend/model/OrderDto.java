package cz.karotkin.eshopbackend.model;

import lombok.*;

import java.time.Instant;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class OrderDto {

    private Long id;
    private Instant createdAt;
    private OrderStatus status;
    private List<ProductSimpleDto> products;
}
