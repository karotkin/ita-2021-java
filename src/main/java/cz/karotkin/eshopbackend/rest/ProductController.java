package cz.karotkin.eshopbackend.rest;

import cz.karotkin.eshopbackend.model.PreviewResponse;
import cz.karotkin.eshopbackend.model.ProductRequestDto;
import cz.karotkin.eshopbackend.model.ProductDto;
import cz.karotkin.eshopbackend.model.ProductSimpleDto;
import cz.karotkin.eshopbackend.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("api/v1/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping("{id}")
    public ProductDto findProduct(@PathVariable("id") Long id) {
        return productService.findProduct(id);
    }

    @GetMapping
    public Collection<ProductSimpleDto> findAllProducts() {
        return productService.findAllProducts();
    }

    @PostMapping
    public ProductDto createProduct(@Valid @RequestBody ProductRequestDto productRequestDto) {
        return productService.createProduct(productRequestDto);
    }

    @PutMapping("{id}")
    public ProductDto editProduct(@PathVariable("id") Long id, @Valid @RequestBody ProductRequestDto productRequestDto) {
        return productService.editProduct(id, productRequestDto);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProduct(@PathVariable("id") Long id) {
        productService.deleteProduct(id);
    }

    @PostMapping(value = "{id}/preview", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void addPreview(@PathVariable Long id, @RequestPart("file") MultipartFile file) {
        productService.addPreview(id, file);
    }

    @GetMapping(value = "{id}/preview", produces = MediaType.APPLICATION_PDF_VALUE)
    public byte[] getPreview(@PathVariable("id") Long id, HttpServletResponse response) {
        PreviewResponse previewResponse = productService.getPreview(id);
        response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=" + previewResponse.getFilename());
        return previewResponse.getBytes();
    }

}
