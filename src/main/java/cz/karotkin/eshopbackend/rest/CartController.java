package cz.karotkin.eshopbackend.rest;

import cz.karotkin.eshopbackend.model.CartDto;
import cz.karotkin.eshopbackend.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("api/v1/carts")
@RequiredArgsConstructor
public class CartController {

    private final CartService cartService;

    @PostMapping("/products/{id}")
    public CartDto createCart(@PathVariable("id") Long productId) {
        return cartService.createCart(productId);
    }

    @PutMapping("/{cartId}/products/{productId}")
    public CartDto updateCart(@PathVariable("cartId") Long cartId, @PathVariable("productId") Long productId) {
        return cartService.updateCart(cartId, productId);
    }
}
