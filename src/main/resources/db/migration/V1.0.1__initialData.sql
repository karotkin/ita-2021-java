INSERT INTO author (id, created_at, modified_at, bio, birth_date, name)
VALUES (1, null, null, 'Narozen v Písku', '1986-04-13', 'Dominik Bican'),
       (2, null, null, 'Velmi dobrý spisovatel.', '1990-03-22', 'Lenka Bicanová');

INSERT INTO genre (id, created_at, modified_at, description, name)
VALUES (1, null, null, 'Detektivní žánr', 'Detektivka'),
       (2, null, null, 'Romantický žánr', 'Román');