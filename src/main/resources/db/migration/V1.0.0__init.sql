create table author
(
    id          int8 not null,
    created_at  timestamp(9),
    modified_at timestamp(9),
    bio         varchar(255),
    birth_date  date,
    name        varchar(255),
    primary key (id)
);
create table cart
(
    id          int8 not null,
    created_at  timestamp(9),
    modified_at timestamp(9),
    primary key (id)
);
create table eshop_order
(
    id          int8 not null,
    created_at  timestamp(9),
    modified_at timestamp(9),
    status      varchar(255),
    primary key (id)
);
create table genre
(
    id          int8 not null,
    created_at  timestamp(9),
    modified_at timestamp(9),
    description varchar(255),
    name        varchar(255),
    primary key (id)
);
create table product
(
    id          int8 not null,
    created_at  timestamp(9),
    modified_at timestamp(9),
    description varchar(512),
    image       varchar(255),
    name        varchar(255),
    price       int8,
    stock       int8,
    author_id   int8,
    genre_id    int8,
    primary key (id)
);
create table r_cart_product
(
    cart_id    int8 not null,
    product_id int8 not null
);
create table r_order_product
(
    order_id   int8 not null,
    product_id int8 not null
);
create sequence author_seq start with 1000 increment by 1;
create sequence cart_seq start with 1000 increment by 1;
create sequence genre_seq start with 1000 increment by 1;
create sequence order_seq start with 1000 increment by 1;
create sequence product_seq start with 1000 increment by 1;
alter table product
    add constraint fk_product_author foreign key (author_id) references author;
alter table product
    add constraint fk_product_genre foreign key (genre_id) references genre;
alter table r_cart_product
    add constraint fk_cart_product_product foreign key (product_id) references product;
alter table r_cart_product
    add constraint fk_cart_product_cart foreign key (cart_id) references cart;
alter table r_order_product
    add constraint fk_order_product_product foreign key (product_id) references product;
alter table r_order_product
    add constraint fk_order_product_order foreign key (order_id) references eshop_order;